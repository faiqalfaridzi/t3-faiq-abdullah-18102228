<?php
echo strlen("Hello world!"); // outputs 12

echo "<hr>";

echo str_word_count("Hello world!"); // outputs 2

echo "<hr>";

echo strrev("Hello world!"); // outputs !dlrow olleH

echo "<hr>";

echo strpos("Hello world!", "world"); // outputs 6

echo "<hr>";

echo str_replace("world", "Dolly", "Hello world!"); // outputs Hello Dolly!
?>